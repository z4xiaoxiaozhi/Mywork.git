package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {
    Button mybutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("lifeline",this.getLocalClassName()+"创建");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        jumpTomain();
    }

    @Override
    protected void onStart() {
        Log.d("lifeline",this.getLocalClassName()+"start");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("lifeline",this.getLocalClassName()+"停止");
        super.onStop();
    }
    public void jumpTomain(){
        mybutton=findViewById(R.id.juptomain);

        mybutton.setOnClickListener(new myonclicklistener());



    }
    class myonclicklistener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent();
            //通过intent对象调用setclass设定源activity和目标activity
            intent.setClass(MainActivity2.this,MainActivity.class);
            MainActivity2.this.startActivity(intent);
            //加入如下代码前面一个activity跳转后会被kill掉
             MainActivity2.this.finish();
        }
    }
}
