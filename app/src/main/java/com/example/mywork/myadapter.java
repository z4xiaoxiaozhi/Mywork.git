package com.example.mywork;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;


public class myadapter extends RecyclerView.Adapter <myadapter.myviewholder>{
    private ArrayList<HashMap<String,Object>> data;
    private Context context;
    private View inflater;
    public myadapter(Context context,ArrayList<HashMap<String,Object>> data) {
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         inflater = LayoutInflater.from(context).inflate(R.layout.contactlist, parent, false);
        myviewholder myholder = new myviewholder(inflater);
        return myholder;
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position) {
        HashMap<String, Object> stringObjectHashMap = data.get(position);
        holder.m.setImageResource(R.drawable.contact);

       // String[] strings = stringObjectHashMap.keySet().toArray(new String[0]);
//        System.out.println(strings.toString());
//        System.out.println(strings[0]);
        holder.name.setText(stringObjectHashMap.get("name").toString());
        holder.number.setText(stringObjectHashMap.get("number").toString());
        holder.type.setText(stringObjectHashMap.get("type").toString());

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class myviewholder extends RecyclerView.ViewHolder {
        ImageButton m;
        TextView name,number,type;
        public myviewholder(@NonNull View itemView) {
            super(itemView);
            m=itemView.findViewById(R.id.peoimg);
            name=itemView.findViewById(R.id.contactname);
            number=itemView.findViewById(R.id.contactnumber);
            type=itemView.findViewById(R.id.contacttype);
            //m.setOnClickListener(new MainActivity.myonclicklistener());
            m.setOnClickListener(new myonclicklistener());
        }
    }
    class myonclicklistener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent();
            //通过intent对象调用setclass设定源activity和目标activity
            intent.setClass(context,MainActivity3.class);

            ArrayList<String> m=new ArrayList<>();
            for (HashMap<String,Object> x:data
                 ) {
                m.add(x.get("name").toString());
            }
            System.out.println(m.toString());
            intent.putExtra("data",m);
            intent.putExtra("test","hhh");
            context.startActivity(intent);
            //加入如下代码前面一个activity跳转后会被kill掉
            //MainActivity3.this.finish();
        }
    }
//    public void jumpToac3(){
//        Bj=findViewById(R.id.peoimg);
//        System.out.println("执行跳转");
//        System.out.println(Bj==null);
//        Bj.setOnClickListener(new MainActivity.myonclicklistener());
//
//    }

}
